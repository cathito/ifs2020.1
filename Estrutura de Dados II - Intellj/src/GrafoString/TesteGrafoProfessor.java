package GrafoString;


public class TesteGrafoProfessor {

    public static void main(String[] args) {
        System.out.println("\n                  ESTRUTURA DE DADOS II - GRAFO!");

        Grafo grafo =  new Grafo();
/*================================================================================================================*/

        System.out.println("\n                   Inserindo Vertices no Grafo: \n");

        grafo.criarVertice(10);
        grafo.criarVertice(20);
        grafo.criarVertice(30);
        grafo.criarVertice(40);
        grafo.criarVertice(50);
        grafo.criarVertice(60);
        grafo.criarVertice(70);
        grafo.criarVertice(80);
        grafo.criarVertice(90);
        grafo.criarVertice(100);
        grafo.criarVertice(110);
        grafo.criarVertice(120);
        grafo.criarVertice(130);
        grafo.criarVertice(140);
        grafo.criarVertice(150);
        grafo.criarVertice(160);
        grafo.criarVertice(170);
        grafo.criarVertice(180);
        grafo.criarVertice(190);
        grafo.criarVertice(200);

        System.out.println("\n          Imprimindo os Elementos que Compoem o Grafo:");
        System.out.println(grafo.mapaGrafo.keySet());

        System.out.println("\n================================================================\n");

/*================================================================================================================*/


        System.out.println("\n                   Inserindo Apontador Para o Vertice: \n");


        grafo.criarApontador(10,20 , 1,2,1);
        grafo.criarApontador(10,60, 3,2,1);
        grafo.listarArestaDoElemento(10);

        System.out.println("\n==================================================================================\n");

        grafo.criarApontador(20,10, 2,3,5);
        grafo.criarApontador(20,30, 1,2,1);
        grafo.listarArestaDoElemento(20);

        System.out.println("\n==================================================================================\n");

        grafo.criarApontador(30,20,3,2,1);
        grafo.criarApontador(30,40,3,2,1);
        grafo.listarArestaDoElemento(30);

        System.out.println("\n==================================================================================\n");

        grafo.criarApontador(40,30,5,2,1);
        grafo.criarApontador(40,50,1,1,1);
        grafo.listarArestaDoElemento(40);

        System.out.println("\n==================================================================================\n");

        grafo.criarApontador(50,100,1,1,1);
        grafo.listarArestaDoElemento(50);

        System.out.println("\n==================================================================================\n");


        grafo.criarApontador(60,70,1,1,1);
        grafo.listarArestaDoElemento(60);

        System.out.println("\n==================================================================================\n");

        grafo.criarApontador(70,10,3,2,5);
        grafo.criarApontador(70,60,1,1,1);
        grafo.criarApontador(70,80,1,1,1);
        grafo.criarApontador(70,120,1,7,8);
        grafo.listarArestaDoElemento(70);

        System.out.println("\n==================================================================================\n");

        grafo.criarApontador(80,70,1,1,1);
        grafo.criarApontador(80,90,1,1,1);
        grafo.listarArestaDoElemento(80);

        System.out.println("\n==================================================================================\n");


        grafo.criarApontador(90,140,3,2,1);
        grafo.listarArestaDoElemento(90);

        System.out.println("\n==================================================================================\n");


        grafo.criarApontador(100,90,10,8,7);
        grafo.criarApontador(100,150,1,1,1);
        grafo.listarArestaDoElemento(100);

        System.out.println("\n==================================================================================\n");


        grafo.criarApontador(110,120,1,1,1);
        grafo.criarApontador(110,160,3,2,1);
        grafo.listarArestaDoElemento(110);

        System.out.println("\n==================================================================================\n");


        grafo.criarApontador(120,70,1,1,1);
        grafo.criarApontador(120,110,1,1,1);
        grafo.criarApontador(120,130,1,1,1);
        grafo.criarApontador(120,170,1,1,1);
        grafo.listarArestaDoElemento(120);

        System.out.println("\n==================================================================================\n");

        grafo.criarApontador(130,120,1,1,1);
        grafo.criarApontador(130,140,1,1,1);
        grafo.criarApontador(130,180,10,7,3);
        grafo.listarArestaDoElemento(130);

        System.out.println("\n==================================================================================\n");

        grafo.criarApontador(140,90,9,9,9);
        grafo.criarApontador(140,130,1,1,1);
        grafo.criarApontador(140,150,1,1,1);
        grafo.listarArestaDoElemento(140);

        System.out.println("\n==================================================================================\n");

        grafo.criarApontador(150,140,1,1,1);
        grafo.criarApontador(150,200,1,6,1);
        grafo.listarArestaDoElemento(150);

        System.out.println("\n==================================================================================\n");

        grafo.criarApontador(160,110,3,2,1);
        grafo.criarApontador(160,170,1,1,1);
        grafo.listarArestaDoElemento(160);

        System.out.println("\n==================================================================================\n");

        grafo.criarApontador(170,120,1,1,1);
        grafo.criarApontador(170,160,1,1,1);
        grafo.criarApontador(170,180,3,2,1);
        grafo.listarArestaDoElemento(170);

        System.out.println("\n==================================================================================\n");

        grafo.criarApontador(180,120,1,7,8);
        grafo.criarApontador(180,170,1,6,1);
        grafo.criarApontador(180,190,1,1,1);
        grafo.listarArestaDoElemento(180);

        System.out.println("\n==================================================================================\n");

        grafo.criarApontador(190,180,1,1,1);
        grafo.criarApontador(190,200,1,1,1);
        grafo.listarArestaDoElemento(190);

        System.out.println("\n==================================================================================\n");

        grafo.criarApontador(200,190,1,1,1);
        grafo.listarArestaDoElemento(200);

        System.out.println("\n==================================================================================\n");


/*================================================================================================================*/

        /*System.out.println("                 Meta 01 - Verificar se o Caminho Existe!\n");

        grafo.buscarCaminhos(100,200);


        System.out.println("\n==================================================================================\n");


        System.out.println("                  Meta 02 - Pegar o Menor Caminho Existe!");

        grafo.buscarMenorCaminho(100,200);



        System.out.println("\n==================================================================================\n");
*/
        System.out.println("        Meta 03 - Pegar o Melhor Caminho Existe Baseado nos Pesos!\n");
        grafo.buscarMelhorCaminho(10,200);

        System.out.println("\n------------------ ooooooooooo ------------------\n");

        grafo.buscarMelhorCaminho2(10,200);





    }

}
