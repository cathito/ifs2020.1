package GrafoString;

import java.util.ArrayList;

public class Vertice {
    int dado;
    ArrayList <Aresta> arestas = new ArrayList <Aresta>();

//==========================================================================//

    public Vertice(int dado){
        this.dado = dado;
    }

//==========================================================================//

    public void adicionarAresta(Vertice destino){
        Aresta arresta = new Aresta( destino);
        arestas.add(arresta);
    }

//==========================================================================//

    public void adicionarPesos(Vertice destino, int peso1, int peso2, int peso3){
        Aresta arresta = bucarAresta(destino);
        arresta.adicionarPeso(peso1, peso2, peso3);
    }

//==========================================================================//

    public Aresta bucarAresta(Vertice destino){
        for(int i = 0; i <= arestas.size(); i++) {
            if(arestas.get(i).destino.dado==destino.dado)
                return arestas.get(i);
        }
        return null;
    }

// ==========================================================================//

    public void listarAresta(){
        System.out.print("Esta Apontando Para os Vertices: [ ");
        for(int i = 0; i <arestas.size(); i++) {
            String pesos = arestas.get(i).listarPeso();
            System.out.print(arestas.get(i).destino.dado + "(" + pesos + "), ");
        }
        System.out.print("].  <==> Modelo [Vertice(Pesos)].\n");
    }

//==========================================================================//


}
