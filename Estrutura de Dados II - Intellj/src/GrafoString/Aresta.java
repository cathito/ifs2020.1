package GrafoString;

public class Aresta {
    Vertice destino;
    Peso peso;
//==========================================================================//

    public Aresta(Vertice destino){
        this.destino=destino;
    }

 //==========================================================================//

    public void adicionarPeso(int peso1, int peso2, int peso3){
        peso = new Peso(peso1, peso2, peso3);
    }

//==========================================================================//

    public void listarPesos(){
        System.out.println(peso.toString());
    }


    public String listarPeso(){
        try {
            return peso.toString();
        }catch(NullPointerException e){
            return "-";
        }
    }

//==========================================================================//

}
