package GrafoString;

public class Peso {

    private int Peso1, Peso2, Peso3;

    public int getPeso1() {
        return Peso1;
    }

    public int getPeso2() {
        return Peso2;
    }

    public int getPeso3() {
        return Peso3;
    }
//==========================================================================//

    public Peso(int peso1){
        Peso1=peso1;
    }

    public Peso(int peso1, int peso2){
        this(peso1);
        Peso2=peso2;
    }

    public Peso(int peso1, int peso2, int peso3){
        this(peso1,peso2);
        Peso3=peso3;
    }
//==========================================================================//

    @Override
    public String toString() {
        return Peso1 + ", " + Peso2 + ", " + Peso3;
    }

//==========================================================================//
    public float media(){
        float soma =(getPeso1()+getPeso2()+getPeso3())/3;
        return soma;
    }


}
