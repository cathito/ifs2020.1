package GrafoString;

public class TesteGrafo {

    public static void main(String[] args) {
        System.out.println("\n                  ESTRUTURA DE DADOS II - GRAFO!");

        Grafo grafo =  new Grafo();
/*================================================================================================================*/

        System.out.println("\n                   Inserindo Vertices no Grafo: \n");

        grafo.criarVertice(10);
        grafo.criarVertice(20);
        grafo.criarVertice(30);
        grafo.criarVertice(40);
        grafo.criarVertice(50);
        grafo.criarVertice(60);

        System.out.println("\n          Imprimindo os Elementos que Compoem o Grafo:");
        System.out.println(grafo.mapaGrafo.keySet());

        System.out.println("\n================================================================\n");

/*================================================================================================================*/


        System.out.println("\n                   Inserindo Apontador Para o Vertice: \n");


        grafo.criarApontador(10,20);
        grafo.criarApontador(10,40);
        grafo.criarApontador(10,50);

        grafo.listarArestaDoElemento(10);

        System.out.println("\n==================================================================================\n");

        grafo.criarApontador(20,30);
        grafo.criarApontador(20,50);
        grafo.criarApontador(20,60);

        grafo.listarArestaDoElemento(20);

        System.out.println("\n==================================================================================\n");

        grafo.criarApontador(30,60);
        grafo.listarArestaDoElemento(30);

        System.out.println("\n==================================================================================\n");

        grafo.criarApontador(40,50);
        //grafo.criarApontador(40,10);
        grafo.listarArestaDoElemento(40);

        System.out.println("\n==================================================================================\n");
        grafo.criarApontador(50,60);
        grafo.listarArestaDoElemento(50);

        System.out.println("\n==================================================================================\n");

/*================================================================================================================/

        System.out.println("\n                   Inserindo Pesos Entre Vertices: \n");

        grafo.criarPesos(10,40, 1, 3, 5 );

        grafo.listarArestaDoElemento(10);

        System.out.println("\n==================================================================================\n");

/*================================================================================================================*/


        System.out.println("                 Meta 01 - Verificar se o Caminho Existe!\n");

        //grafo.buscarCaminho(10,10);
        //grafo.buscarCaminho(10,100);

        //grafo.buscarCaminho(10,50);
        grafo.buscarCaminhos(10,60);

        System.out.println("\n==================================================================================\n");

        System.out.println("                  Meta 02 - Pegar o Menor Caminho Existe!");

        grafo.buscarMenorCaminho(10,60);

        /*
        System.out.println("\n==================================================================================\n");

        System.out.println("        Meta 02 - Pegar o Menor Caminho Existe Baseado nos Pesos!");


        System.out.println(grafo.buscarVertice(60).arestas.size());

        */
    }

}
