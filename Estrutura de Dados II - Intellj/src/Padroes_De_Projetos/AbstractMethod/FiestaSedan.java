package Padroes_De_Projetos.AbstractMethod;

public class FiestaSedan implements CarroSedan {

    @Override
    public void exibirInfoSedan() {
        System.out.println("Fabricante: Ford - Modelo: FiestaSedan - Categoria: Sedan");
    }

}
