package Padroes_De_Projetos.AbstractMethod;

public class Palio implements CarroPopular {

    @Override
    public void exibirInfoPopular() {
        System.out.println("Fabricante: Fiat - Modelo: Palio - Categoria: Popular");
    }

}
