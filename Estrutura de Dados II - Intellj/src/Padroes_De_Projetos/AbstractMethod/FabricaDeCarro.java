package Padroes_De_Projetos.AbstractMethod;


public interface FabricaDeCarro {

    CarroSedan criarCarroSedan();
    CarroPopular criarCarroPopular();

}
