package Padroes_De_Projetos.AbstractMethod;

public class Fiesta implements CarroPopular {

    @Override
    public void exibirInfoPopular() {
        System.out.println("Fabricante: Ford - Modelo: Fiesta - Categoria: Popular");
    }

}
