package Padroes_De_Projetos.AbstractMethod;

public class Siena implements CarroSedan {

    @Override
    public void exibirInfoSedan() {
        System.out.println("Fabricante: Fiat - Modelo: Siena - Categoria: Sedan"); //segmento
    }

}
