package Padroes_De_Projetos.FactoryMethod;

public class FabricaFiat implements FabricaDeCarro {

    @Override
    public Carro criarCarro() { // e se uma mesma fabrica Produzir doi carros diferentes
        return new Palio();
    }

}
