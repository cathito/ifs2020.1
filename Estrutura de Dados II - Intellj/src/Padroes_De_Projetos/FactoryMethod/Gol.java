package Padroes_De_Projetos.FactoryMethod;

public class Gol implements Carro {

    @Override
    public void exibirInfo() {
        System.out.println("Fabricante: Volkswagen -  Modelo: Gol");
    }

}
