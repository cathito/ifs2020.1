package Padroes_De_Projetos.FactoryMethod;

public class Palio implements Carro {

    @Override
    public void exibirInfo() {
        System.out.println("Fabricante: Fiat - Modelo: Palio");
    }

}
