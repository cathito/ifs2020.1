package Padroes_De_Projetos.FactoryMethod;

public class FabricaChevrolet implements FabricaDeCarro {

    @Override
    public Carro criarCarro() {
        return new Celta();
    }

}
