package Padroes_De_Projetos.FactoryMethod;

public class Fiesta implements Carro {

    @Override
    public void exibirInfo() {
        System.out.println("Fabricante: Ford -  Modelo: Fiesta");
    }

}
