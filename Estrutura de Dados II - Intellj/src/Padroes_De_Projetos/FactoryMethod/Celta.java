package Padroes_De_Projetos.FactoryMethod;

public class Celta implements Carro {

    @Override
    public void exibirInfo() {
        System.out.println("Fabricante: Chevrolet - Modelo: Celta");
    }

}
