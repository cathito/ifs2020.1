package Padroes_De_Projetos.Builder;

import java.util.Scanner;

public class Teste {
    public static void main(String args[]) {
        Scanner input = new Scanner(System.in);

        System.out.println("                        Implementacao do Padrao de Projeto - BUILDER");

        System.out.println("\n             * CARDAPIO PATTERN_BURGERS: ");
        System.out.println("   - HAMBURGUERS:         - Hamburguer - // - Cheeseburger -");
        System.out.println("   - TAMANHO DA BATATA:   - Pequena - // - Media - // - Grande -");
        System.out.println("   - REFRIGERANTE:        - Coca - // - Guarana -");
        System.out.println("   - BRINQUEDO:           - Carrinho - // - Boneca -\n\n");


        Atendente atendente = new Atendente();
        Montador montador = new Montador();

        atendente.criaPedido(montador,"Hamburguer", "Pequena", "Carrinho", "Guarana");

        atendente.imprimirPedido();

    }
}
