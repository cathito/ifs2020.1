package Padroes_De_Projetos.Builder;

public class Atendente {

    private PedidoBuilder pedidoBuilder;


    public Pedido getPedido() {
        return pedidoBuilder.getPedido();
    }


    public void criaPedido(Montador montador, String ham, String bat, String brin, String refri) {
        pedidoBuilder = montador;

        pedidoBuilder.criaNovoPedido();
        pedidoBuilder.fazHamburguer(ham);
        pedidoBuilder.fazBatata(bat);
        pedidoBuilder.fazBrinquedo(brin);
        pedidoBuilder.porRefrigerante(refri);
    }


    public void imprimirPedido(){
        System.out.println(getPedido().toString());
    }


}
