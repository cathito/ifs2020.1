package Padroes_De_Projetos.Builder;

public class Montador extends PedidoBuilder {

    @Override
    public void fazHamburguer(String tipoHamburger) {
        pedido.adicionarDentroDaCaixa(tipoHamburger);
    }

    @Override
    public void fazBatata(String tamanhoBatata) {
        pedido.adicionarDentroDaCaixa("Batata - Porcao " +tamanhoBatata);
    }

    @Override
    public void fazBrinquedo(String brinde) {
        pedido.adicionarDentroDaCaixa(brinde);
    }

    @Override
    public void porRefrigerante(String refrigerante) {
        pedido.adicionarForaDaCaixa(refrigerante);
    }


}
