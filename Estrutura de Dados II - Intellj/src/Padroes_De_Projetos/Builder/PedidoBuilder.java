package Padroes_De_Projetos.Builder;

public abstract class PedidoBuilder {

    protected Pedido pedido;

    public Pedido getPedido() {
        return pedido;
    }

    public void criaNovoPedido() {
        pedido = new Pedido();
    }

    public abstract void fazHamburguer(String tipoHamburger);
    public abstract void fazBatata(String tamanhoBatata);
    public abstract void fazBrinquedo(String brinde);
    public abstract void porRefrigerante(String brinde);
}
