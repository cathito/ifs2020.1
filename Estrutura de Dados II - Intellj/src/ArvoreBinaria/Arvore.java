package ArvoreBinaria;

public class Arvore {
	
	NoArvore raiz;

// ----------------------------------//
	//protected int altramMaxima = 0;


	public boolean arvoreVazia() {
		return raiz == null;
	}

	public boolean subArvoreVazia(NoArvore subRaiz) {
		return subRaiz == null;
	}

	public boolean folha(NoArvore subRaiz) {
		return subRaiz.esquerdo == null && subRaiz.direito==null;
	}

	public boolean umFoilho(NoArvore noDeveraSerRemovido) {
		return noDeveraSerRemovido.esquerdo != null && noDeveraSerRemovido.direito != null;
	}

// ==============================================================================================================//
// ==============================================================================================================//

	public void adicionarValor(int dado) {
		adicionarValor(raiz, dado);
		System.out.println("O Valor: " + dado + ", foi Adicionando a Arvore!");
	}

	private NoArvore adicionarValor(NoArvore subRaiz, int dado) {

		if (subRaiz == null) {
			subRaiz = new NoArvore(dado);

			if (raiz == null)
				raiz = subRaiz;

		} else if (dado < subRaiz.dado) {
			subRaiz.esquerdo = adicionarValor(subRaiz.esquerdo, dado);

		} else {
			subRaiz.direito = adicionarValor(subRaiz.direito, dado);
		}

		return subRaiz;
	}

//==============================================================================================================//
//==============================================================================================================//

	public void buscarValor(int valor) {

		System.out.print("\nEm Busca do Valor: " + valor + "...  -  ");
		if( busca(raiz, valor) != null) {
			System.out.println("Valor encontrado!");
		}else {
			System.out.println("Valor N�o Encontrado!");
		}

	}


	protected NoArvore busca(NoArvore subRaiz, int valor) {

		if (subRaiz == null) {
			return null;
		} else {
			if (valor < subRaiz.dado) {
				return busca(subRaiz.esquerdo, valor);
			} else if (valor > subRaiz.dado){
				return busca(subRaiz.direito, valor);
			}
			return subRaiz;
		}

	}


// ==============================================================================================================//
// ==============================================================================================================//


	public void Altura() {

        if (arvoreVazia()) {
			System.out.println("A Arvore esta Vazia. Logo, a Mesma N�o Possui Altura!");
        } else {
            int alturaEncontrada = Altura(raiz, 0,0);

            System.out.println("\nNa linguagem de maquina a altura da arvore �: " + alturaEncontrada);
            System.out.println("Na linguagem humana a altura da arvore �: " + (alturaEncontrada + 1) + "\n");
            //this.altramMaxima = 0;
        }
	}

/*/ --------------------------------------------------------------------------//

	private int Altura(NoArvore subRaiz, int alturaAtual) {

		if (subRaiz.esquerdo != null) {
			Altura(subRaiz.esquerdo, alturaAtual+1);
		}
		if (subRaiz.direito != null) {
			Altura(subRaiz.direito, alturaAtual+1);
		}

		if (alturaAtual > altramMaxima) {
			altramMaxima = alturaAtual;
		}

		return altramMaxima;
	}*/


	private int Altura(NoArvore subRaiz, int alturaAtual, int alturaMaxima) {
		int alturaEncontrada = 0;

		if (subRaiz.esquerdo != null) {
			alturaEncontrada = Altura(subRaiz.esquerdo, alturaAtual+1, alturaMaxima);
			if (alturaEncontrada > alturaMaxima) {
				alturaMaxima = alturaEncontrada;
			}

		} if (subRaiz.direito != null) {
			alturaEncontrada = Altura(subRaiz.direito, alturaAtual+1, alturaMaxima);
			if (alturaEncontrada > alturaMaxima) {
				alturaMaxima = alturaEncontrada;
			}
		}

		if (alturaAtual > alturaMaxima) {
			alturaMaxima = alturaAtual;
		}

		return alturaMaxima;
	}
// --------------------------------------------------------------------------//

	public void numeroFolhas() {
		if (arvoreVazia()) {
			System.out.println("A Arvore esta Vazia. Logo, a Mesma N�o Possui Folhas a Serem Contabilizadas!");
		} else {
			System.out.println("A arvore possui um limite maximo de " + ((int) Math.pow(2, Altura(raiz, 0,0))) + ", folhas!");
			//this.altramMaxima = 0;
		}
	}
// --------------------------------------------------------------------------//

	public void totalElementos() {
		if (arvoreVazia()) {
			System.out.println("A Arvore esta Vazia. Logo, a Mesma N�o Possui Elementos a Serem Contabilizados!");
		} else {
			System.out.println("A arvore possui um limite maximo de " + (((int) Math.pow(2, Altura(raiz, 0,0) + 1)) - 1) + ", elementos!");
			//this.altramMaxima = 0;
		}
	}

// ==============================================================================================================//
// ==============================================================================================================//

	public void PercorrerEmLargura() {
		if (arvoreVazia()) {
			System.out.println("A Arvore esta Vazia. Logo, a Mesma N�o Possui Elementos a Serem Percorridods!");
		} else {
			FilaSE fila = new FilaSE();
			fila.Adicionar(raiz);
			PercorrerEmLargura(fila, fila.getInicio().dado);
		}
	}

	private void PercorrerEmLargura(FilaSE fila, NoArvore noAtual) {
		System.out.println(noAtual.dado);

		if (noAtual.esquerdo != null) {
			fila.Adicionar(noAtual.esquerdo);
		}
		if (noAtual.direito != null) {
			fila.Adicionar(noAtual.direito);
		}
		fila.Remover();

		if (fila.getInicio() != null) {
			PercorrerEmLargura(fila, fila.getInicio().dado);
		}

	}

// ==============================================================================================================//
// ==============================================================================================================//


	public void removerPorValor(int valor) {

		removerPorNO(null, raiz, valor);
		System.out.println("O Elemento "+valor+" foi Removido com Sucesso!");

	}


	private NoArvore removerPorNO(NoArvore noPai, NoArvore noFilho_Atual, int valor) {

		if(noFilho_Atual==null){
			System.out.println("N�o � Possivel Realizar a Remo��o do Valor "+valor+", Pois o Mesmo N�o Foi Encontrado!");
			return null;
		}

		if (valor < noFilho_Atual.dado) {
			removerPorNO(noFilho_Atual, noFilho_Atual.esquerdo, valor); // relizando busca

		} else if (valor > noFilho_Atual.dado) {
			removerPorNO(noFilho_Atual, noFilho_Atual.direito, valor); // relizando busca

		} else { // valor que devera ser removido foi encontrado
			noFilho_Atual=removaNO(noPai, noFilho_Atual);
		}

		return noFilho_Atual;
	}



	private NoArvore removaNO(NoArvore noPai, NoArvore noFilho_Atual){
		if(noPai==null){ //estarei trabalhando com a raiz
			if (folha(noFilho_Atual)) {
				return raiz = null;
			}else{
				if (!umFoilho(noFilho_Atual)) { //a raiz possui um filho
					if (noFilho_Atual.esquerdo!= null) { //apagar a referencia do filho que devera ser removido
						return raiz= noFilho_Atual.esquerdo;
					} else {
						return raiz= noFilho_Atual.direito;
					}

				}else{ //a raiz possui dois filho
					if (noFilho_Atual.direito.esquerdo != null) {
						NoArvore folha = pegarFolha(noFilho_Atual.direito.esquerdo);
						removerPorNO(noFilho_Atual.direito, noFilho_Atual.direito.esquerdo,folha.dado);
						noFilho_Atual.dado = folha.dado;
						return noFilho_Atual;

					} else {
						noFilho_Atual.direito.esquerdo=raiz.esquerdo;
						return raiz = noFilho_Atual.direito;
					}
				}
			}

		}else { //Pai e diferente de null, entao n estamos trabalhando na raiz

			if (folha(noFilho_Atual)) {  // NO � uma folha, logo a remova
				if (noPai.esquerdo == noFilho_Atual) { //apagar a referencia do filho que devera ser removido
					return noPai.esquerdo = null;
				} else {
					return noPai.direito = null;
				}

			} else {

				if (!umFoilho(noFilho_Atual)) { // O noFilho_Atual possui um filho

					if (noFilho_Atual.esquerdo != null) { // encontar o lado do neto

						if (noPai.esquerdo == noFilho_Atual) {
							noPai.esquerdo = noFilho_Atual.esquerdo;
						} else {
							noPai.direito = noFilho_Atual.esquerdo;
						}
						noFilho_Atual.esquerdo = null; //para q o removido perca todas associacoes com a arvore

					} else {

						if (noPai.esquerdo == noFilho_Atual) {
							noPai.esquerdo = noFilho_Atual.direito;
						} else {
							noPai.direito = noFilho_Atual.direito;
						}
						noFilho_Atual.direito = null; //para q o removido perca todas associacoes com a arvore

					}
					return noFilho_Atual;

				}else{ // O noFilho_Atual possui dois filhos

					if (noFilho_Atual.direito.esquerdo != null) {
						NoArvore folha = pegarFolha(noFilho_Atual.direito.esquerdo);
						removerPorNO(noFilho_Atual.direito, noFilho_Atual.direito.esquerdo,folha.dado);
						noFilho_Atual.dado = folha.dado;
						return noFilho_Atual;

					} else {
						noFilho_Atual.direito.esquerdo=noFilho_Atual.esquerdo;

						if (noPai.esquerdo == noFilho_Atual) {
							noPai.esquerdo = noFilho_Atual.direito;
							noFilho_Atual.direito = null;
							return noPai.esquerdo;
						} else {
							noPai.direito = noFilho_Atual.direito;
							noFilho_Atual.direito = null;
							return noPai.direito;
						}
					}

				} //pai n � raiz, possui Dois filhos

			}//pai n � raiz, possui filhos

		} //pai n � raiz
	}

/*--------------------------------------------------------------------------------------------*/

	private NoArvore pegarFolha(NoArvore raiz) {
		
		if (raiz.esquerdo != null) {
			return pegarFolha(raiz.esquerdo);
		
		}else if (raiz.direito != null) {
			return pegarFolha(raiz.direito);
		}

		return raiz;
	}



// ==============================================================================================================//
// ==============================================================================================================//
	


	public void emOrdem() {
		imprimiOrdenado(raiz);
	}

	private void imprimiOrdenado(NoArvore noAtual) {
		if (noAtual != null) {
			imprimiOrdenado(noAtual.esquerdo);
			System.out.print(noAtual.dado + ", ");
			imprimiOrdenado(noAtual.direito);
		}
	}

// -----------------------------------------------------//
	public void emOrdeminverso() {
		imprimiOrdenadoinverso(raiz);
	}

	private void imprimiOrdenadoinverso(NoArvore noAtual) {
		if (noAtual != null) {
			imprimiOrdenado(noAtual.direito);
			System.out.print(noAtual.dado + ", ");
			imprimiOrdenado(noAtual.esquerdo);
		}
	}

// ==============================================================================================================//
// ==============================================================================================================//

	

}
