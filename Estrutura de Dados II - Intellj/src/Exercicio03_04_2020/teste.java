package Exercicio03_04_2020;

public class teste {

    public static void main(String[] args) {
        Arvore pinheiro = new Arvore();

        System.out.println("\n                  ESTRUTURA DE DADOS II!");

        System.out.println("\n                Inserindo Dados Na Arvore: \n");

        pinheiro.add(500);
        pinheiro.add(300);
        pinheiro.add(600);
        pinheiro.add(550);
        pinheiro.add(650);
        pinheiro.add(700);
        pinheiro.add(200);
        pinheiro.add(400);
        pinheiro.add(100);
        pinheiro.add(250);
        pinheiro.add(350);
        pinheiro.add(450);
        pinheiro.add(150);
        pinheiro.add(690);

        System.out.println("\n=================================================================");


        System.out.println("\n\n                   Percorrer em Largura!");
        pinheiro.PercorrerEmLargura();
        System.out.println("\nImprimindo os Dados que Compoem a Arvore de Forma Ordenada: ");
        pinheiro.emOrdem();
        System.out.println("\n----------------------------------------------------------------");


        System.out.println("\n                 Dimensoes e Capacidade!");

        pinheiro.Altura();

        pinheiro.numeroFolhas();

        pinheiro.totalElementos();


        System.out.println("\n=================================================================");


        System.out.println("\n                   Busca por Elemento!");

        pinheiro.buscarValor(1);

        pinheiro.buscarValor(10);

        pinheiro.buscarValor(100);

        pinheiro.buscarValor(150);

        pinheiro.buscarValor(250);

        pinheiro.buscarValor(400);

        pinheiro.buscarValor(600);

        pinheiro.buscarValor(1000);


        System.out.println("\n\n=================================================================");


        System.out.println("\n\n                   Removendo de Elemento!\n");


        pinheiro.remover(690);


        System.out.println("\n=================================================================");


        System.out.println("\n\n                   Percorrer em Largura!");
        pinheiro.PercorrerEmLargura();

        System.out.println("\n           Elementos que Compoe a Arvore: \n   ");
        pinheiro.emOrdem();


        System.out.println("\n=================================================================");

        System.out.println("\n\n                   Removendo de Elemento!\n");

        pinheiro.remover(300);


        System.out.println("\n=================================================================");

        System.out.println("\n\n                   Percorrer em Largura!");
        pinheiro.PercorrerEmLargura();

        System.out.println("\n           Elementos que Compoe a Arvore: \n   ");
        pinheiro.emOrdem();


        System.out.println("\n=================================================================");
//=========================================================================================================================//

        /* // ====== Questao do professor ====== //

        Arvore questao = new Arvore();

        //(a+b*(c+d)*e)
         *
        */

//=========================================================================================================================//



    }


}
