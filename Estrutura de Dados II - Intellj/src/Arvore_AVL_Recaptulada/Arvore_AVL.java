package Arvore_AVL_Recaptulada;

public class Arvore_AVL extends Arvore{

//============================================================================================//

	public void limparArvore(){
		raiz = null;
	}

	public void adicionarValor(int dado) {
		NoArvore novo_NO = new NoArvore(dado);

		if (raiz == null)
			raiz = novo_NO;
		else
			adicionarValor(raiz, novo_NO);

		System.out.println("Valor: " + dado + ", foi Adicionando a Arvore AVL!");
	}


	private NoArvore adicionarValor(NoArvore NO_Atual, NoArvore novo_NO) {

		if (NO_Atual == null) {
			NO_Atual = novo_NO;

		} else if (novo_NO.dado < NO_Atual.dado) {
			NO_Atual.esquerdo = adicionarValor(NO_Atual.esquerdo, novo_NO);
		} else {
			NO_Atual.direito = adicionarValor(NO_Atual.direito, novo_NO);
		}

		if(!folha(NO_Atual) ){ //caso seja uma folha nao precisa balancear //ate aqui ta ok
			NoArvore aposBalanceamento = balancearArvore(NO_Atual);

			if(NO_Atual==raiz) {
				return raiz = aposBalanceamento;
			}
			NO_Atual = aposBalanceamento;
		}
		return NO_Atual;
	}

//============================================================================================//
//============================================================================================//

	public void arvoreEstaBalanceada(){
		if(fatorDeBalanceamento(raiz) <=-2 && fatorDeBalanceamento(raiz) >= 2){
			System.out.println("Arvore Balanceada!");
		}else{
			System.out.println("Arvore Desbalanceada!");
		}
	}


	public NoArvore balancearArvore(NoArvore subRaiz){
		int fatorBalanceamento = fatorDeBalanceamento(subRaiz);

		if(fatorBalanceamento <=-2) { //rotaciona a direita
			System.out.println("Fator de Balanceamento = "+ fatorBalanceamento +". Ser� Necessario Rotacionar Para a Direita!");
			subRaiz = rotacaoParaDireita(subRaiz);

		}else if(fatorBalanceamento >= 2){ //rotaciona a esquerda
			System.out.println("Fator de Balanceamento = "+ fatorBalanceamento +". Ser� Necessario Rotacionar Para a Esquerda!");
			subRaiz = rotacaoParaEsquerda(subRaiz);
		}
		return subRaiz;
	}


	public int fatorDeBalanceamento(NoArvore subRaiz){

		if(subRaiz==null){
			return 0;
		}
		return alturaSubArvore(subRaiz.direito) - (alturaSubArvore(subRaiz.esquerdo));
	}

// ==============================================================================================================//

	public int alturaSubArvore(NoArvore subRaiz) {
		if (subArvoreVazia(subRaiz)) {
			return -1;
		} else {
			return Altura(subRaiz, 0, 0);
		}
	}


// ==============================================================================================================//
// ==============================================================================================================//


	public NoArvore rotacaoParaDireita(NoArvore iraRotacionar){
		if (iraRotacionar.esquerdo.esquerdo != null) { // --- Verificado - Rotacao Simples

            if(iraRotacionar.esquerdo.direito!=null && !folha(iraRotacionar.esquerdo.direito)){ //outra rotacao dupla
                if(!possuiDoisFoilhos(iraRotacionar.esquerdo.direito)){
                	return iraRotacionar = rotacaoDuplaParaDireita(iraRotacionar);
				}
            }
            iraRotacionar = rotacaoSimplesParaDireita(iraRotacionar);
			return iraRotacionar;
		}else{
             return iraRotacionar = rotacaoDuplaParaDireita(iraRotacionar);
		}
	}


    public NoArvore rotacaoSimplesParaDireita(NoArvore iraRotacionar){
        NoArvore salveItem = iraRotacionar.esquerdo.direito;

        NoArvore novoNo = iraRotacionar.esquerdo;
        novoNo.direito = iraRotacionar;
        novoNo.direito.esquerdo = salveItem;
		iraRotacionar = novoNo;
        return iraRotacionar;
    }

    public NoArvore rotacaoDuplaParaDireita(NoArvore iraRotacionar){
        iraRotacionar.esquerdo = rotacaoSimplesParaEsquerda(iraRotacionar.esquerdo);
        return rotacaoSimplesParaDireita(iraRotacionar);
    }



// ==============================================================================================================//
// ==============================================================================================================//

	public NoArvore rotacaoParaEsquerda(NoArvore iraRotacionar){

		if (iraRotacionar.direito.direito != null) { // ---------- Verificado
			if(iraRotacionar.direito.esquerdo!=null && !folha(iraRotacionar.direito.esquerdo)){ //outra rotacao dupla
				if(!possuiDoisFoilhos(iraRotacionar.direito.esquerdo)){
					return iraRotacionar = rotacaoDuplaParaEsquerda(iraRotacionar);
				}
			}
            return iraRotacionar = rotacaoSimplesParaEsquerda(iraRotacionar); // rotacao +simples
		}else{
			return iraRotacionar = rotacaoDuplaParaEsquerda(iraRotacionar);
		}
	}


    public NoArvore rotacaoSimplesParaEsquerda(NoArvore iraRotacionar){
        NoArvore salveItem = iraRotacionar.direito.esquerdo;

        NoArvore novoNo = iraRotacionar.direito;
        novoNo.esquerdo = iraRotacionar;
        novoNo.esquerdo.direito = salveItem;

        return iraRotacionar = novoNo;
    }

    public NoArvore rotacaoDuplaParaEsquerda(NoArvore iraRotacionar){
        iraRotacionar.direito = rotacaoSimplesParaDireita(iraRotacionar.direito);
        return rotacaoSimplesParaEsquerda(iraRotacionar);
    }

// ==============================================================================================================//
// ==============================================================================================================//

	public void removerPorValor(int valor) {
		if(arvoreVazia()){
			System.out.println("N�o � Possivel Remover Elementos de Uma Arvore Que Esta Vazia!");
		}else{
			raiz = removerPorNO(null, raiz, valor);
			System.out.println("O Elemento "+valor+" Foi Removido Com Sucesso!");
		}
	}


	private NoArvore removerPorNO(NoArvore noPai, NoArvore noFilho_Atual, int valor) {

		if(noFilho_Atual==null){
			System.out.println("N�o � Possivel Realizar a Remo��o do Valor "+valor+", Pois o Mesmo N�o Foi Encontrado!");
			return null;
		}

		if (valor < noFilho_Atual.dado) {
            removerPorNO(noFilho_Atual, noFilho_Atual.esquerdo, valor); // relizando busca

		} else if (valor > noFilho_Atual.dado) {
            removerPorNO(noFilho_Atual, noFilho_Atual.direito, valor); // relizando busca

		} else { // valor que devera ser removido foi encontrado
            noFilho_Atual = removaElemento(noPai, noFilho_Atual);
		}

        try { // proxima tarefa, elininar essa excessao
            if (noPai.direito == noFilho_Atual)
                noPai.direito = balancearArvore(noFilho_Atual);
            else
                noPai.esquerdo = balancearArvore(noFilho_Atual);
        }catch (NullPointerException e){
            //System.out.println("entrou na excessao!");
        }

        return noFilho_Atual;
	}



	private NoArvore removaElemento(NoArvore noPai, NoArvore noFilho_Atual){
		if(noPai == null){
			return noFilho_Atual = remocaoPaiNulo(noFilho_Atual);
		}
		return noFilho_Atual = remocaoTendoPai( noPai, noFilho_Atual);
	}


	private NoArvore remocaoPaiNulo(NoArvore noFilho_Atual){
		if(folha(noFilho_Atual)){
			return raiz = null;
		}else{
			if (!possuiDoisFoilhos(noFilho_Atual)) { //a raiz possui um filho
				if (noFilho_Atual.esquerdo!= null) { //apagar a referencia do filho que devera ser removido
					return raiz= noFilho_Atual.esquerdo;
				} else {
					return raiz= noFilho_Atual.direito;
				}

			}else{ //a raiz possui dois filho
				if (noFilho_Atual.direito.esquerdo != null) {
					NoArvore folha = pegarFolha(noFilho_Atual.direito.esquerdo);
					removerPorNO(noFilho_Atual, noFilho_Atual.direito,folha.dado);
					noFilho_Atual.dado = folha.dado;

					return noFilho_Atual;

				} else {
					noFilho_Atual.direito.esquerdo = noFilho_Atual.esquerdo;
					return noFilho_Atual = noFilho_Atual.direito;
				}
			}
		}

	}


	private NoArvore remocaoTendoPai(NoArvore noPai, NoArvore noFilho_Atual) {

		if (folha(noFilho_Atual)) {  // NO � uma folha, logo a remova
			if (noPai.esquerdo == noFilho_Atual) { //apagar a referencia do filho que devera ser removido
				return noPai.esquerdo = null;
			} else {
				return noPai.direito = null;
			}

		} else {

			if (!possuiDoisFoilhos(noFilho_Atual)) { // O noFilho_Atual possui um filho

				if (noFilho_Atual.esquerdo != null) {
					return noFilho_Atual=noFilho_Atual.esquerdo;
				}else{
					return noFilho_Atual=noFilho_Atual.direito;
				}

			} else { // O noFilho_Atual possui dois filhos

				if (noFilho_Atual.direito.esquerdo != null) {
					NoArvore folha = pegarFolha(noFilho_Atual.direito.esquerdo);
					removerPorNO(noFilho_Atual, noFilho_Atual.direito, folha.dado);
					noFilho_Atual.dado = folha.dado;
					return noFilho_Atual;

				} else {
					noFilho_Atual.direito.esquerdo = noFilho_Atual.esquerdo;

					if (noPai.esquerdo == noFilho_Atual) {
						noPai.esquerdo = noFilho_Atual.direito;
						noFilho_Atual.direito = null;
						return noPai.esquerdo;
					} else {
						noPai.direito = noFilho_Atual.direito;
						noFilho_Atual.direito = null;
						return noPai.direito;
					}
				}

			} //pai n � raiz, possui Dois filhos

		}//pai n � raiz, possui filhos
	}

/*========================================================================================================*/
    protected NoArvore pegarFolha(NoArvore raiz) {
        try {
            if (raiz.esquerdo != null) {
                return pegarFolha(raiz.esquerdo);

            } else if (raiz.direito != null) {
                return pegarFolha(raiz.direito);
            }

            return raiz;
        }catch (NullPointerException e){
            return null;
        }
    }




}
