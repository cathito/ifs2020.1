package Arvore_AVL_Recaptulada;

public class teste_ArvoreAVL {

    public static void main(String[] args) {
    	Arvore_AVL Nemeton = new Arvore_AVL();

        System.out.println("\n                  ESTRUTURA DE DADOS II - ARVORE-AVL!");

/*
        System.out.println("\n\n\n//================================================================================\\\\");
        System.out.println(      "     //  //  //  //  //  //  //   INSERCAO DE DADOS   // //  //  //  //  //  //  ");
        System.out.println(    "\\\\================================================================================// \n");

        System.out.println("\n INSERCAO DE DADOS - TESTE DE NIVEL 1");


        System.out.println("\n                Insercao que  Busca Uma Rotacao a Direita");
        System.out.println("                  Inserindo Dados Na Arvore Binaria AVL: \n");
        Nemeton.adicionarValor(30);
        Nemeton.adicionarValor(20);
        Nemeton.adicionarValor(10);

        System.out.println("\n                  Percorrer em Largura!");
        Nemeton.PercorrerEmLargura();

        System.out.println("                    Arvores Impressa!");
        System.out.println("\n             Elementos que Compoe a Arvore de Forma Ordenada: \n   ");
        Nemeton.emOrdem();
        Nemeton.limparArvore();

        System.out.println("\n=========================================================================\n");


        System.out.println("\n                Insercao que  Busca Uma Rotacao Dupla Esquerda-Direita");
        System.out.println("                  Inserindo Dados Na Arvore Binaria AVL: \n");
        Nemeton.adicionarValor(30);
        Nemeton.adicionarValor(10);
        Nemeton.adicionarValor(20);

        System.out.println("\n                  Percorrer em Largura!");
        Nemeton.PercorrerEmLargura();

        System.out.println("                    Arvores Impressa!");
        System.out.println("\n             Elementos que Compoe a Arvore de Forma Ordenada: \n   ");
        Nemeton.emOrdem();
        Nemeton.limparArvore();

        System.out.println("\n=========================================================================\n");



        System.out.println("\n                Insercao que  Busca Uma Rotacao a Esquerda");
        System.out.println("                  Inserindo Dados Na Arvore Binaria AVL: \n");
        Nemeton.adicionarValor(10);
        Nemeton.adicionarValor(20);
        Nemeton.adicionarValor(30);

        System.out.println("\n                  Percorrer em Largura!");
        Nemeton.PercorrerEmLargura();

        System.out.println("                    Arvores Impressa!");
        System.out.println("\n             Elementos que Compoe a Arvore de Forma Ordenada: \n   ");
        Nemeton.emOrdem();

        Nemeton.limparArvore();
        System.out.println("\n=========================================================================\n");


        System.out.println("\n                Insercao que  Busca Uma Rotacao Direita-Esquerda");
        System.out.println("                  Inserindo Dados Na Arvore Binaria AVL: \n");
        Nemeton.adicionarValor(10);
        Nemeton.adicionarValor(30);
        Nemeton.adicionarValor(20);

        System.out.println("                  Percorrer em Largura!");
        Nemeton.PercorrerEmLargura();

        System.out.println("                    Arvores Impressa!");
        System.out.println("\n             Elementos que Compoe a Arvore de Forma Ordenada: \n   ");
        Nemeton.emOrdem();
        Nemeton.limparArvore();

        System.out.println("\n\n==============================================================================\n" +
                             "  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  ");
          System.out.println("==============================================================================\n\n");


        System.out.println(" INSERCAO DE DADOS - TESTE DE NIVEL 2 ");


        System.out.println("\n                Insercao que  Busca Uma Rotacao a Esquerda");
        System.out.println("                  Inserindo Dados Na Arvore Binaria AVL: \n");
        Nemeton.adicionarValor(50);
        Nemeton.adicionarValor(30);
        Nemeton.adicionarValor(70);
        Nemeton.adicionarValor(60);
        Nemeton.adicionarValor(80);
        Nemeton.adicionarValor(90);

        System.out.println("\n                  Percorrer em Largura!");
        Nemeton.PercorrerEmLargura();

        System.out.println("                    Arvores Impressa!");
        System.out.println("\n             Elementos que Compoe a Arvore de Forma Ordenada: \n   ");
        Nemeton.emOrdem();
        Nemeton.limparArvore();

        System.out.println("\n=========================================================================\n");


        System.out.println("\n                Insercao que  Busca Uma Rotacao a Direita");
        System.out.println("                  Inserindo Dados Na Arvore Binaria AVL: \n");
        Nemeton.adicionarValor(50);
        Nemeton.adicionarValor(30);
        Nemeton.adicionarValor(70);
        Nemeton.adicionarValor(10);
        Nemeton.adicionarValor(40);
        Nemeton.adicionarValor(5);

        System.out.println("\n                  Percorrer em Largura!");
        Nemeton.PercorrerEmLargura();

        System.out.println("                    Arvores Impressa!");
        System.out.println("\n             Elementos que Compoe a Arvore de Forma Ordenada: \n   ");
        Nemeton.emOrdem();
        Nemeton.limparArvore();


        System.out.println("\n\n==============================================================================\n" +
                "  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  ");
        System.out.println("==============================================================================\n\n");


        System.out.println(" INSERCAO DE DADOS - TESTE DE NIVEL 3 ");


        System.out.println("\n                Insercao que  Busca Uma Rotacao a Esquerda");
        System.out.println("                  Inserindo Dados Na Arvore Binaria AVL: \n");
        Nemeton.adicionarValor(50);
        Nemeton.adicionarValor(30);
        Nemeton.adicionarValor(70);
        Nemeton.adicionarValor(10);
        Nemeton.adicionarValor(40);
        Nemeton.adicionarValor(35);

        System.out.println("\n                  Percorrer em Largura!");
        Nemeton.PercorrerEmLargura();

        System.out.println("                    Arvores Impressa!");
        System.out.println("\n             Elementos que Compoe a Arvore de Forma Ordenada: \n   ");
        Nemeton.emOrdem();
        Nemeton.limparArvore();

        System.out.println("\n=========================================================================\n");

        System.out.println("\n                Insercao que  Busca Uma Rotacao a Direita");
        System.out.println("                  Inserindo Dados Na Arvore Binaria AVL: \n");
        Nemeton.adicionarValor(70);
        Nemeton.adicionarValor(50);
        Nemeton.adicionarValor(80);
        Nemeton.adicionarValor(71);
        Nemeton.adicionarValor(90);
        Nemeton.adicionarValor(75);


        System.out.println("                  Percorrer em Largura!");
        Nemeton.PercorrerEmLargura();

        System.out.println("\n\n                    Arvores Impressa!");
        System.out.println("\n             Elementos que Compoe a Arvore de Forma Ordenada: \n   ");
        Nemeton.emOrdem();
        Nemeton.limparArvore();


        System.out.println("\n\n\n//================================================================================\\\\");
        System.out.println(      "     //  //  //  //  //  //   DIMENSIONAMENTO DE DADOS   //  //  //  //  //  //  ");
        System.out.println(    "\\\\================================================================================// \n");


        Nemeton.adicionarValor(500);
        Nemeton.adicionarValor(300);
        Nemeton.adicionarValor(550);
        Nemeton.adicionarValor(600);
        Nemeton.adicionarValor(200);
        Nemeton.adicionarValor(400);
        Nemeton.adicionarValor(100);
        Nemeton.adicionarValor(250);
        Nemeton.adicionarValor(350);
        Nemeton.adicionarValor(450);
        Nemeton.adicionarValor(150);
        Nemeton.adicionarValor(540);
        Nemeton.adicionarValor(545);

        System.out.println("\n                  Percorrer em Largura!");
        Nemeton.PercorrerEmLargura();

        System.out.println("                    Arvores Impressa!");
        System.out.println("\n             Elementos que Compoe a Arvore de Forma Ordenada: \n   ");
        Nemeton.emOrdem();

        System.out.println("\n\n                 Dimensoes e Capacidade!");

        Nemeton.Altura();

        Nemeton.numeroFolhas();

        Nemeton.totalElementos();

        System.out.println("\n\n\n//================================================================================\\\\");
        System.out.println(      "    //  //  //  //  //  //  // //  BUSCA DE DADOS  // // //  //  //  //  //  //  ");
        System.out.println(    "\\\\================================================================================// \n");

        System.out.println("\n\n                   Busca por Elemento!");

        Nemeton.buscarValor(10);

        Nemeton.buscarValor(100);

        Nemeton.buscarValor(350);

        Nemeton.buscarValor(400);

        Nemeton.buscarValor(600);

        Nemeton.buscarValor(1000);
        Nemeton.limparArvore();
*/

        System.out.println("\n\n\n//================================================================================\\\\");
        System.out.println(      "     //  //  //  //  //  //   REMOCAO DE DADOS   //  //  //  //  //  //  ");
        System.out.println(    "\\\\================================================================================// \n");


        // Folha removida com sucesso! ----------------- ok
        // Com um Filho removido com sucesso! ---------- ok
        // Com dois Filho removido com sucesso! -------- ok


        Nemeton.adicionarValor(5);
        /*Nemeton.adicionarValor(3);
        Nemeton.adicionarValor(8);
        Nemeton.adicionarValor(2);
        Nemeton.adicionarValor(4);
        Nemeton.adicionarValor(7);
        Nemeton.adicionarValor(10);
        Nemeton.adicionarValor(1);
        Nemeton.adicionarValor(6);
        Nemeton.adicionarValor(9);
        Nemeton.adicionarValor(11);*/


        System.out.println("\n                  Percorrer em Largura!");
        Nemeton.PercorrerEmLargura();

        System.out.println("                    Arvores Impressa!");
        System.out.println("\n             Elementos que Compoe a Arvore de Forma Ordenada: \n   ");
        Nemeton.emOrdem();

        System.out.println("\n=================================================================");


        System.out.println("\n REMOCAO DE DADOS - TESTE DE NIVEL 1\n");

        Nemeton.removerPorValor(5);
       // Nemeton.removerPorValor(8);
        //Nemeton.removerPorValor(9);


        System.out.println("\n=================================================================");

        System.out.println("\n\n                  Percorrer em Largura!");
        Nemeton.PercorrerEmLargura();

        System.out.println("\n\n                    Arvores Impressa!");
        System.out.println("\n             Elementos que Compoe a Arvore de Forma Ordenada: \n   ");
        Nemeton.emOrdem();

        System.out.println("\n=================================================================");

/*        System.out.println("\n\n                 Dimensoes e Capacidade!");

        Nemeton.altura();

        Nemeton.numeroFolhas();

        Nemeton.totalElementos();

        System.out.println("\n\n=================================================================");



///=========================================================================================================================//

        /* // ====== Questao do professor ====== //

        Arvore questao = new Arvore();

        //(a+b*(c+d)*e)
         
        System.out.println(
				"Removido o Valor: " + valor + "\n\nImprimindo os Dados que Compoem a Arvore apos a remocao: ");
		emOrdem();
		System.out.println("\n----------------------------------------------------------------\n");

        */

//=========================================================================================================================//


    }


}
