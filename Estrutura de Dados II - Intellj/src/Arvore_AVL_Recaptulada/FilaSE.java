package Arvore_AVL_Recaptulada;

public class FilaSE {
	
	private NoFila inicio;
	private NoFila fim;
	int qtdListas=0;
	
	public NoFila getInicio() {
		return inicio;
	}
//----------------------------------------------------//

	public void Adicionar(NoArvore valor) {
		NoFila novo = new NoFila(valor);

		if (inicio == null){
			this.inicio = novo;
			this.fim=inicio;
		}else {
			fim.proximo=novo;
			fim=novo;
		}
		qtdListas++;
		
	}
//----------------------------------------------------//
	public void Remover() {
		NoFila Atual= inicio;
		if(Atual!=null){
			inicio=Atual.proximo;
			qtdListas--;
		}

	}
//----------------------------------------------------//
	public void Listar() {
		NoFila novo= inicio;
		while(novo!=null) {
			System.out.println("| "+novo.dado+" |");
			novo=novo.proximo;
		}
	}
//----------------------------------------------------//
	
	
	

}
