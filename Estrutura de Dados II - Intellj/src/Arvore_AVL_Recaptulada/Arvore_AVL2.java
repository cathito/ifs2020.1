package Arvore_AVL_Recaptulada;

public class Arvore_AVL2 extends Arvore{

//============================================================================================//

	public void limparArvore(){
		raiz = null;
	}

	public void adicionarValor(int dado) {
		NoArvore novo_NO = new NoArvore(dado);

		if (raiz == null)
			raiz = novo_NO;
		else
			adicionarValor(raiz, novo_NO);

		System.out.println("Valor: " + dado + ", foi Adicionando a Arvore AVL! \n");
	}


	private NoArvore adicionarValor(NoArvore NO_Atual, NoArvore novo_NO) {

		if (NO_Atual == null) {
			NO_Atual = novo_NO;

		} else if (novo_NO.dado < NO_Atual.dado) {
			NO_Atual.esquerdo = adicionarValor(NO_Atual.esquerdo, novo_NO);
		} else {
			NO_Atual.direito = adicionarValor(NO_Atual.direito, novo_NO);
		}

		if(!folha(NO_Atual) ){ //caso seja uma folha nao precisa balancear //ate aqui ta ok
			NoArvore aposBalanceamento = balanceada(NO_Atual);

			if(NO_Atual==raiz) {
				return raiz = aposBalanceamento;
			}
			NO_Atual = aposBalanceamento;
		}
		return NO_Atual;
	}

//============================================================================================//
//============================================================================================//

	public void balanceada(){
		if(fatorDeBalanceamento(raiz) <=-2 && fatorDeBalanceamento(raiz) >= 2){
			System.out.println("Arvore Balanceada!");
		}else{
			System.out.println("Arvore Desbalanceada!");
		}
	}


	public NoArvore balanceada(NoArvore subRaiz){

		int fatorBalanceamento = fatorDeBalanceamento(subRaiz);

		if(fatorBalanceamento <=-2) { //rotaciona a direita
			System.out.println("Fator de Balanceamento = "+ fatorBalanceamento +". Ser� Necessario Rotacionar Para a Direita!");
			subRaiz = rotacaoParaDireita(subRaiz);

		}else if(fatorBalanceamento >= 2){ //rotaciona a esquerda
			System.out.println("Fator de Balanceamento = "+ fatorBalanceamento +". Ser� Necessario Rotacionar Para a Esquerda!");
			subRaiz = rotacaoParaEsquerda(subRaiz);
		}

		return subRaiz;
	}


	public int fatorDeBalanceamento(NoArvore subRaiz){
		if(subRaiz==null){
			return 0;
		}
		return alturaSubArvore(subRaiz.direito) - (alturaSubArvore(subRaiz.esquerdo));
	}

// ==============================================================================================================//

	public int alturaSubArvore(NoArvore subRaiz) {
		if (subArvoreVazia(subRaiz)) {
			return -1;
		} else {
			return Altura(subRaiz, 0, 0);
		}
	}


// ==============================================================================================================//
// ==============================================================================================================//


	public NoArvore rotacaoParaDireita(NoArvore iraRotacionar){

		NoArvore novoNO = iraRotacionar.direito;
		iraRotacionar.direito = novoNO.esquerdo;
		novoNO.esquerdo = iraRotacionar;
		return novoNO;
	}


	public NoArvore rotacaoParaEsquerda(NoArvore iraRotacionar){

		NoArvore x = iraRotacionar.esquerdo;
		iraRotacionar.esquerdo = x.direito;
		x.direito = iraRotacionar;
		return x;

	}


// ==============================================================================================================//
// ==============================================================================================================//

	public void removerPorValor(int valor) {

		if(arvoreVazia()){
			System.out.println("N�o � Possivel Remover Elementos de Uma Arvore Que Esta Vazia!");
		}else{
			removerPorNO(null, raiz, valor);
			System.out.println("O Elemento "+valor+" Foi Removido Com Sucesso!");
		}
	}


	private NoArvore removerPorNO(NoArvore noPai, NoArvore noFilho_Atual, int valor) {

		if(noFilho_Atual==null){
			System.out.println("N�o � Possivel Realizar a Remo��o do Valor "+valor+", Pois o Mesmo N�o Foi Encontrado!");
			return null;
		}

		if (valor < noFilho_Atual.dado) {
			removerPorNO(noFilho_Atual, noFilho_Atual.esquerdo, valor);// relizando busca

		} else if (valor > noFilho_Atual.dado) {
			removerPorNO(noFilho_Atual, noFilho_Atual.direito, valor);// relizando busca

		} else { // valor que devera ser removido foi encontrado
			//noFilho_Atual = removaNO(noPai, noFilho_Atual);
		}

		return balanceada(noFilho_Atual);//ira verificar o balanceamento apos a remocao
	}






}
