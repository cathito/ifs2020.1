package Grafos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class Grafo {
    Map <Integer, Vertice> mapaGrafo = new HashMap <Integer, Vertice>();
/*================================================================================================================*/

    public void criarVertice(int dado){
        mapaGrafo.put( dado, new Vertice(dado) );
        System.out.println("O Vertice Com o Dado "+dado+", Foi Criado e Adicionado ao Grafo!");
    }

/*================================================================================================================*/

    public void criarApontador(int dadoOrigem, int dadoDestino){
        Vertice verticeOrigem = buscarVertice(dadoOrigem);
        Vertice verticeDestino = buscarVertice(dadoDestino);

        if(verticeOrigem==null || verticeDestino==null){
            System.out.println("Nao Foi Possivel Realizar a Operacao! \nO Elemento de Origem e/ou de Destino Nao Foi/Foram Encontrado(s)!");
        }else{
            verticeOrigem.arestas.add(new Aresta( verticeDestino));
            System.out.println("O Vertice Com o Dado "+dadoOrigem+", Foi Apto a Apontar Para o Vertice Com o Dado "+dadoDestino+
                    ". Os Pesos Nao Foram Apresentados!");
        }
    }

/*================================================================================================================*/

    public void criarPesos(int dadoOrigem, int dadoDestino, int peso1, int peso2, int peso3){
        Vertice verticeOrigem = buscarVertice(dadoOrigem);
        Vertice verticeDestino = buscarVertice(dadoDestino);

        if(verticeOrigem==null || verticeDestino==null){
            System.out.println("Não Foi Possivel Realizar a Operação! \nO Elemento de Origem e/ou de Destino Não Foi/Foram Encontrado(s)!");
        }else{
            verticeOrigem.adicionarPesos(verticeDestino, peso1, peso2, peso3);
            System.out.println("Foram Adicionados os Pesos: "+ peso1 + ", " + peso2 + ", " + peso3 +
                    ", na Ligacao Entre o Vertice "+dadoOrigem + " e o Vertice "+dadoDestino+".");
        }

    }

/*================================================================================================================*/

    public void criarApontador(int dadoOrigem, int dadoDestino, int peso1, int peso2, int peso3){
        Vertice verticeOrigem = buscarVertice(dadoOrigem);
        Vertice verticeDestino = buscarVertice(dadoDestino);

        if(verticeOrigem==null || verticeDestino==null){
            System.out.println("Nao Foi Possivel Realizar a Operacao! \nO Elemento de Origem e/ou de Destino Nao Foi/Foram Encontrado(s)!");
        }else{
            Aresta aresta = new Aresta(verticeDestino);
            aresta.adicionarPeso(peso1,peso2,peso3);

            verticeOrigem.arestas.add(aresta);
            System.out.println("O Vertice Com o Dado "+dadoOrigem+", Foi Apto a Apontar Para o Vertice Com o Dado "+dadoDestino+
                    ". Apresentando os Referidos Pesos: "+peso1+", "+peso2+", "+peso3);
        }
    }
/*================================================================================================================*/

    public Vertice buscarVertice(int dado){ //busca esta sendo realizada com metodo pq se a busca n for assim e so resscrever o metodo
        return mapaGrafo.get(dado);//pega o endereco de memoria
    }

/*================================================================================================================*/

    public void listarArestaDoElemento(int dado){
        Vertice verticeOrigem = buscarVertice(dado);

        if(verticeOrigem != null) {
            System.out.println("\n                      O Vertice Com o Dado "+dado+".");
            verticeOrigem.listarAresta();
        }else
            System.out.println("Não Foi Possivel Realizar a Operação! \nO Elemento Composto Pelo"+ dado +"Não Foi Encontrado!");
    }

/*================================================================================================================*/
/*================================================================================================================*/



    public void buscarCaminhos(int dadoOrigem, int dadoDestino){
        Vertice verticeOrigem = buscarVertice(dadoOrigem);
        Vertice verticeDestino = buscarVertice(dadoDestino);

        System.out.println("Ao Partindo do Vertice "+dadoOrigem+"  Desejo Chegar no Vertice "+ dadoDestino+":");

        if(verticeOrigem==null || verticeDestino==null){
            System.out.println("      - Nao Foi Possivel Realizar a Operacao!" +
                    "\n      - O Elemento de Origem e/ou de Destino Nao Foi/Foram Encontrado(s)!\n");

        }else{
            if(verticeOrigem.dado==verticeDestino.dado)
                System.out.println("      - A Origem e o Destino Sao os Mesmos, Logo Nao Podemos Especificar um Caminho!\n");

            else{
                ArrayList <String> caminhosEncontrados = buscarCaminhos(verticeOrigem,verticeDestino,"", new ArrayList<String>(), new ArrayList<Integer>());
                listarCaminhosEncontrados(caminhosEncontrados);
            }
        }

    }


    private ArrayList <String> buscarCaminhos(Vertice verticeAtual, Vertice verticeDestino, String caminho, ArrayList <String> caminhos, ArrayList <Integer> historico){
        caminho = caminho + verticeAtual.dado + " => ";

        for (int i = 0; i < verticeAtual.arestas.size(); i++) {
            if (verticeAtual.arestas.get(i).destino.dado == verticeDestino.dado) {
                caminhos.add(caminho + verticeDestino.dado + " -");
            }else {
                if (!estaEmHistorico(historico, verticeAtual.arestas.get(i).destino.dado)) {
                    historico.add(verticeAtual.dado);
                    buscarCaminhos(verticeAtual.arestas.get(i).destino, verticeDestino, caminho, caminhos, historico);
                }
            }
        }
        int t = historico.size()-1;
        if(t>=0)
            historico.remove(t);
        return caminhos;
    }

/*================================================================================================================*/



    public boolean estaEmHistorico(ArrayList <Integer> historico, int verticeAtual){
        for(int i = 0; i <historico.size(); i++) {
            if(historico.get(i)==verticeAtual)
                return true;
        }
        return false;
    }

//--------------------------------------------------------------------------------------//

    public void listarCaminhosEncontrados(ArrayList <String> caminhosEncontrados){
        for(int i = 0; i <caminhosEncontrados.size(); i++) {
            System.out.println("      - "+caminhosEncontrados.get(i));
        }
    }

/*================================================================================================================*/

    public void buscarMenorCaminho(int dadoOrigem, int dadoDestino){
        System.out.println("\nPartindo do Vertice " + dadoOrigem + " Desejo Encontrar o Melhor Caminho Para o Vertice " + dadoDestino + ":");

        Vertice verticeOrigem = buscarVertice(dadoOrigem);
        Vertice verticeDestino = buscarVertice(dadoDestino);

        if(verticeOrigem==null || verticeDestino==null){
            System.out.println("Não Foi Possivel Realizar a Operação! \nO Elemento de Origem e/ou de Destino Não Foi/Foram Encontrado(s)!");
        }else{
            ArrayList <String> caminhosEncontrados = buscarCaminhos(verticeOrigem,verticeDestino,"", new ArrayList<String>(), new ArrayList<Integer>());
            pegarMenorCaminho(caminhosEncontrados);
        }

    }

//--------------------------------------------------------------------------------------//

    public void pegarMenorCaminho(ArrayList <String> caminhosEncontrados){
        String menorCaminho = null;
        for(int i = 0; i <caminhosEncontrados.size(); i++) {
            if(i==0)
                menorCaminho = caminhosEncontrados.get(i);
            else if(caminhosEncontrados.get(i).length() < menorCaminho.length()){
                menorCaminho = caminhosEncontrados.get(i);
            }
        }

        System.out.println("O Menor Caminho Encontrado Foi Atraves Dos Vertices: \n- " + menorCaminho);
    }

/*================================================================================================================*/

    public void buscarMelhorCaminho(int dadoOrigem, int dadoDestino){
        Vertice verticeOrigem = buscarVertice(dadoOrigem);
        Vertice verticeDestino = buscarVertice(dadoDestino);

        if(verticeOrigem==null || verticeDestino==null){
            System.out.println("Não Foi Possivel Realizar a Operação! \nO Elemento de Origem e/ou de Destino Não Foi/Foram Encontrado(s)!");
        }else{
            ArrayList <String> caminhosEncontrados = buscarCaminhoPesos(verticeOrigem,verticeDestino,"", 0, new ArrayList<String>(), new ArrayList<Integer>());
            listarCaminhosEncontrados(caminhosEncontrados);
            //pegarMelhorCaminho(caminhosEncontrados);
        }
    }


    private ArrayList <String> buscarCaminhoPesos(Vertice verticeAtual, Vertice verticeDestino, String caminho, float pesoCaminho, ArrayList <String> caminhos, ArrayList <Integer> historico){
        caminho = caminho + verticeAtual.dado + " => ";

        for (int i = 0; i < verticeAtual.arestas.size(); i++) {
            if (verticeAtual.arestas.get(i).destino.dado == verticeDestino.dado) {
                pesoCaminho = pesoCaminho + verticeAtual.arestas.get(i).peso.media();
                caminhos.add(caminho + verticeDestino.dado + " - // "+ pesoCaminho);
            }else {
                if (!estaEmHistorico(historico, verticeAtual.arestas.get(i).destino.dado)) {
                    pesoCaminho = pesoCaminho + verticeAtual.arestas.get(i).peso.media();
                    historico.add(verticeAtual.dado);

                    buscarCaminhoPesos(verticeAtual.arestas.get(i).destino, verticeDestino, caminho, pesoCaminho, caminhos, historico);
                }
            }
        }
        int t = historico.size()-1;
        if(t>=0)
            historico.remove(t);
        return caminhos;
    }

//--------------------------------------------------------------------------------------//

    public void pegarMelhorCaminho(ArrayList <String> caminhosEncontrados){
        String menorCaminho = null;
        float pesoTotal = 0;

        for(int i = 0; i <caminhosEncontrados.size(); i++) {
            if(i==0) {
                menorCaminho = caminhosEncontrados.get(i);
                pesoTotal = Float.parseFloat(caminhosEncontrados.get(i).substring(caminhosEncontrados.get(i).indexOf("//")+3));
            }else if(Float.parseFloat(caminhosEncontrados.get(i).substring(caminhosEncontrados.get(i).indexOf("//")+3)) < pesoTotal){
                menorCaminho = caminhosEncontrados.get(i);
                pesoTotal = Float.parseFloat(caminhosEncontrados.get(i).substring(caminhosEncontrados.get(i).indexOf("//")+3));
            }
        }

        System.out.println("\n\nO Menor Caminho Encontrado Foi Atraves Dos Vertices: - " + menorCaminho);
    }

//===============================================================================================================//


    public void buscarMelhorCaminho2(int dadoOrigem, int dadoDestino){
        Vertice verticeOrigem = buscarVertice(dadoOrigem);
        Vertice verticeDestino = buscarVertice(dadoDestino);

        if(verticeOrigem==null || verticeDestino==null){
            System.out.println("Não Foi Possivel Realizar a Operação! \nO Elemento de Origem e/ou de Destino Não Foi/Foram Encontrado(s)!");
        }else{
            String[] caminhoEncontrado = buscarCaminhoPorMenorPeso(verticeOrigem,verticeDestino,"", 0, new ArrayList<Integer>(), new String[2]);
            System.out.println("O Melhor Caminho Encontrado Foi Atraves Dos Vertices: "+caminhoEncontrado[0]+" Somando "+caminhoEncontrado[1]+" Pesos!");
        }
    }

    private String [] buscarCaminhoPorMenorPeso(Vertice verticeAtual, Vertice verticeDestino, String caminhoAtual, float pesoCaminhoAtual, ArrayList <Integer> historico, String[] menorCaminho){
        caminhoAtual = caminhoAtual + verticeAtual.dado + " => ";

        for (int i = 0; i < verticeAtual.arestas.size(); i++) {

            if (verticeAtual.arestas.get(i).destino.dado == verticeDestino.dado) {
                pesoCaminhoAtual = pesoCaminhoAtual + verticeAtual.arestas.get(i).peso.media();
                caminhoAtual = caminhoAtual + verticeDestino.dado+" -";

                if(menorCaminho[0]==null){
                    menorCaminho[0] = caminhoAtual;
                    menorCaminho[1] = ""+pesoCaminhoAtual;
                }else{
                    if(Float.parseFloat(menorCaminho[1]) > pesoCaminhoAtual){
                        menorCaminho[0] = caminhoAtual;
                        menorCaminho[1] = ""+pesoCaminhoAtual;
                    }
                }

            }else {
                if ( !estaEmHistorico(historico, verticeAtual.arestas.get(i).destino.dado) ) {
                    pesoCaminhoAtual = pesoCaminhoAtual + verticeAtual.arestas.get(i).peso.media();
                    historico.add(verticeAtual.dado);

                    menorCaminho = buscarCaminhoPorMenorPeso(verticeAtual.arestas.get(i).destino, verticeDestino,caminhoAtual, pesoCaminhoAtual, historico, menorCaminho);
                }
            }
        }

        int t = historico.size()-1;
        if(t>=0)
            historico.remove(t);

        return menorCaminho;
    }

//===============================================================================================================//




}
