package Arvore_AVL;

public class Arvore_AVL2 extends Arvore{
	
	public void add(int dado) {
		add(raiz, dado);
		System.out.println("Valor: " + dado + ", foi Adicionando a Arvore AVL! \n");
	}

	private NoArvore add(NoArvore subRaiz, int dado) {
		if (subRaiz == null) {
			subRaiz = new NoArvore(dado);
			if (raiz == null)
				raiz = subRaiz;

		} else if (dado < subRaiz.dado) {
			subRaiz.esquerdo = add(subRaiz.esquerdo, dado);
		} else {
			subRaiz.direito = add(subRaiz.direito, dado);
		}

		if(!folha(subRaiz) ){
			NoArvore aposBalanceamento = balanceada(subRaiz);

			if(subRaiz==raiz) {
				raiz = aposBalanceamento;
				return raiz;
			}
			subRaiz = aposBalanceamento;
		}
		return subRaiz;
	}
//==========================================================================//

	public NoArvore balanceada(NoArvore subRaiz){
		System.out.println("O Balanceamento Foi Verificado!");
		int fatorBalanceamento = fatorDeBalanceamento(subRaiz);

        if(fatorBalanceamento <=-2) { //rotaciona a direita
            System.out.println("FB = "+ fatorBalanceamento +". Ser� Necessario Rotacionar a Direita!");
            subRaiz = rotacaoDireita(subRaiz);

        }else if(fatorBalanceamento >= 2){ //rotaciona a esquerda
            System.out.println("FB = "+ fatorBalanceamento +". Ser� Necessario Rotacionar a Esquerda!");
			subRaiz = rotacaoEsquerda(subRaiz);
        }

		return subRaiz;
	}

// ==============================================================================================================//


	public NoArvore rotacaoDireita(NoArvore iraRotacionar){

		NoArvore alfa = null;
		NoArvore beta = null;

		NoArvore novoNo= null;

		if (iraRotacionar.esquerdo.esquerdo != null) {
			alfa = iraRotacionar.esquerdo;
			beta = iraRotacionar.esquerdo.direito;

			novoNo = alfa;
			novoNo.direito = iraRotacionar;
			novoNo.direito.esquerdo=beta;

		}else{
			//Simplificando processo de rotacao dupla
			alfa = iraRotacionar.esquerdo.direito;
			beta = iraRotacionar.esquerdo.direito.direito;
			NoArvore alfaEsquerda = alfa.esquerdo;

			novoNo = alfa;
			novoNo.esquerdo = iraRotacionar.esquerdo;
			novoNo.esquerdo.direito = alfaEsquerda;

			novoNo.direito = iraRotacionar;
			novoNo.direito.esquerdo = beta;

		}
		iraRotacionar = novoNo;
		return iraRotacionar;
	}


	public NoArvore rotacaoEsquerda(NoArvore iraRotacionar){

		NoArvore alfa = null;
		NoArvore beta = null;

		NoArvore novoNo= null;

		if (iraRotacionar.direito.direito != null) {
			alfa = iraRotacionar.direito;
			beta = iraRotacionar.direito.esquerdo;

			novoNo = alfa;
			novoNo.esquerdo = iraRotacionar;
			novoNo.esquerdo.direito=beta;

		}else{
			alfa = iraRotacionar.direito.esquerdo;
			NoArvore alfaEsquerda = alfa.esquerdo;
			beta = alfa.direito;

			novoNo = alfa;
			novoNo.esquerdo = iraRotacionar;
			novoNo.direito = iraRotacionar.direito;
			novoNo.direito.esquerdo = beta;

			novoNo.esquerdo.direito = alfaEsquerda;

		}

		return novoNo;
	}


// ==============================================================================================================//

	public int alturaSubArvore(NoArvore subRaiz) {

		if (subArvoreVazia(subRaiz)) {
			return -1;
		} else {
			int alturaEncontrada = altura(subRaiz, 0);
			altramMaxima = 0;
			return alturaEncontrada;
		}
	}

// ==============================================================================================================//

	public void balanceada(){
		if(fatorDeBalanceamento(raiz) <=-2 && fatorDeBalanceamento(raiz) >= 2){
			System.out.println("Arvore Balanceada!");
		}else{
			System.out.println("Arvore Desbalanceada!");
		}
	}

	public int fatorDeBalanceamento(NoArvore subRaiz){
		if(subRaiz==null){
			return 0;
		}

		int alturaEsquerda = alturaSubArvore(subRaiz.esquerdo);
		int alturaDireita = alturaSubArvore(subRaiz.direito);
		int FB = alturaDireita - (alturaEsquerda);

		return FB;
	}

// ==============================================================================================================//
	public void removerPorValor(int valor) {

		if(arvoreVazia()){
			System.out.println("N�o � Possivel Remover Elementos de Uma Arvore Que Esta Vazia!");
		}else{
			removerPorNO(null, raiz, valor);
			System.out.println("O Elemento "+valor+" Foi Removido Com Sucesso!");
		}
	}


	private NoArvore removerPorNO(NoArvore noPai, NoArvore noFilho_Atual, int valor) {

		if(noFilho_Atual==null){
			System.out.println("N�o � Possivel Realizar a Remo��o do Valor "+valor+", Pois o Mesmo N�o Foi Encontrado!");
			return null;
		}

		if (valor < noFilho_Atual.dado) {
			removerPorNO(noFilho_Atual, noFilho_Atual.esquerdo, valor);// relizando busca

		} else if (valor > noFilho_Atual.dado) {
			removerPorNO(noFilho_Atual, noFilho_Atual.direito, valor);// relizando busca

		} else { // valor que devera ser removido foi encontrado
			noFilho_Atual=removaNO(noPai, noFilho_Atual);

		}

		return balanceada(noFilho_Atual);//ira verificar o balanceamento apos a remocao

	}

	private NoArvore removaNO(NoArvore noPai, NoArvore noFilho_Atual){

		if(noPai==null){ //estarei trabalhando com a raiz
			if (noFolha(noFilho_Atual)) {
				return raiz = null;
			}else{
				if (umFoilho(noFilho_Atual)) { //a raiz possui um filho
					if (noFilho_Atual.esquerdo!= null) { //apagar a referencia do filho que devera ser removido
						return raiz= noFilho_Atual.esquerdo;
					} else {
						return raiz= noFilho_Atual.direito;
					}

				}else{ //a raiz possui dois filho
					if (noFilho_Atual.direito.esquerdo != null) {
						NoArvore folha = pegarFolha(noFilho_Atual.direito.esquerdo);
						removerPorNO(noFilho_Atual.direito, noFilho_Atual.direito.esquerdo,folha.dado);
						noFilho_Atual.dado = folha.dado;
						return noFilho_Atual;

					} else {
						noFilho_Atual.direito.esquerdo=raiz.esquerdo;
						return raiz = noFilho_Atual.direito; // obs
					}
				}
			}


		}else { //Pai e diferente de null, entao n estamos trabalhando na raiz

			if (noFolha(noFilho_Atual)) {  // NO � uma folha, logo a remova
				if (noPai.esquerdo == noFilho_Atual) { //apagar a referencia do filho que devera ser removido
					return noPai.esquerdo = null;
				} else {
					return noPai.direito = null;
				}

			} else {

				if (umFoilho(noFilho_Atual)) { // O noFilho_Atual possui um filho

					if (noFilho_Atual.esquerdo != null) { // encontar o lado do neto

						if (noPai.esquerdo == noFilho_Atual) {
							noPai.esquerdo = noFilho_Atual.esquerdo;
						} else {
							noPai.direito = noFilho_Atual.esquerdo;
						}
						noFilho_Atual.esquerdo = null; //para q o removido perca todas associacoes com a arvore

					} else {

						if (noPai.esquerdo == noFilho_Atual) {
							noPai.esquerdo = noFilho_Atual.direito;
						} else {
							noPai.direito = noFilho_Atual.direito;
						}
						noFilho_Atual.direito = null; //para q o removido perca todas associacoes com a arvore

					}
					return noFilho_Atual;

				}else{ // O noFilho_Atual possui dois filhos
					if (noFilho_Atual.direito.esquerdo != null) {
						NoArvore folha = pegarFolha(noFilho_Atual.direito.esquerdo);
						removerPorNO(noFilho_Atual.direito, noFilho_Atual.direito.esquerdo,folha.dado);
						noFilho_Atual.dado = folha.dado;
						return noFilho_Atual;

					} else {
						noFilho_Atual.direito.esquerdo=noFilho_Atual.esquerdo;

						if (noPai.esquerdo == noFilho_Atual) {
							noPai.esquerdo = noFilho_Atual.direito;
							noFilho_Atual.direito = null;
							return noPai.esquerdo;
						} else {
							noPai.direito = noFilho_Atual.direito;
							noFilho_Atual.direito = null;
							return noPai.direito;
						}
					}

				} //pai n � raiz, possui Dois filhos

			}//pai n � raiz, possui filhos

		} //pai n � raiz


	}




}
