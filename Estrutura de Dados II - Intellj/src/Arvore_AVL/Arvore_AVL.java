package Arvore_AVL;

public class Arvore_AVL extends Arvore{
	
	public void add(int dado) {
		add(raiz, dado);
		System.out.println("Valor: " + dado + ", foi Adicionando a Arvore AVL! \n");
	}

	private NoArvore add(NoArvore subRaiz, int dado) {
		if (subRaiz == null) {
			subRaiz = new NoArvore(dado);
			if (raiz == null)
				raiz = subRaiz;

		} else if (dado < subRaiz.dado) {
			subRaiz.esquerdo = add(subRaiz.esquerdo, dado);
		} else {
			subRaiz.direito = add(subRaiz.direito, dado);
		}

		if(!folha(subRaiz) ){
			NoArvore aposBalanceamento = balanceada(subRaiz);

			if(subRaiz==raiz) {
				raiz = aposBalanceamento;
				return raiz;
			}
			subRaiz = aposBalanceamento;
		}
		return subRaiz;
	}
//==========================================================================//

	public NoArvore balanceada(NoArvore subRaiz){
		int fatorBalanceamento = fatorDeBalanceamento(subRaiz);

        if(fatorBalanceamento <=-2) { //rotaciona a direita
            System.out.println("FB = "+ fatorBalanceamento +". Ser� Necessario Rotacionar a Direita!");
            subRaiz = rotacaoDireita(subRaiz);

        }else if(fatorBalanceamento >= 2){ //rotaciona a esquerda
            System.out.println("FB = "+ fatorBalanceamento +". Ser� Necessario Rotacionar a Esquerda!");
			subRaiz = rotacaoEsquerda(subRaiz);
        }

		return subRaiz;
	}

// ==============================================================================================================//


	public NoArvore rotacaoDireita(NoArvore iraRotacionar){

		NoArvore alfa = null;
		NoArvore beta = null;

		NoArvore novoNo= null;

		if (iraRotacionar.esquerdo.esquerdo != null) {
			alfa = iraRotacionar.esquerdo;
			beta = iraRotacionar.esquerdo.direito;

			novoNo = alfa;
			novoNo.direito = iraRotacionar;
			novoNo.direito.esquerdo=beta;

		}else{
			//Simplificando processo de rotacao dupla
			alfa = iraRotacionar.esquerdo.direito;
			beta = iraRotacionar.esquerdo.direito.direito;
			NoArvore alfaEsquerda = alfa.esquerdo;

			novoNo = alfa;
			novoNo.esquerdo = iraRotacionar.esquerdo;
			novoNo.esquerdo.direito = alfaEsquerda;

			novoNo.direito = iraRotacionar;
			novoNo.direito.esquerdo = beta;

		}
		iraRotacionar = novoNo;
		return iraRotacionar;
	}


	public NoArvore rotacaoEsquerda(NoArvore iraRotacionar){

		NoArvore alfa = null;
		NoArvore beta = null;

		NoArvore novoNo= null;

		if (iraRotacionar.direito.direito != null) {
			alfa = iraRotacionar.direito;
			beta = iraRotacionar.direito.esquerdo;

			novoNo = alfa;
			novoNo.esquerdo = iraRotacionar;
			novoNo.esquerdo.direito=beta;

		}else{
			alfa = iraRotacionar.direito.esquerdo;
			NoArvore alfaEsquerda = alfa.esquerdo;
			beta = alfa.direito;

			novoNo = alfa;
			novoNo.esquerdo = iraRotacionar;
			novoNo.direito = iraRotacionar.direito;
			novoNo.direito.esquerdo = beta;

			novoNo.esquerdo.direito = alfaEsquerda;

		}

		return novoNo;
	}


// ==============================================================================================================//

	public int alturaSubArvore(NoArvore subRaiz) {

		if (subArvoreVazia(subRaiz)) {
			return -1;
		} else {
			int alturaEncontrada = altura(subRaiz, 0);
			altramMaxima = 0;
			return alturaEncontrada;
		}
	}

// ==============================================================================================================//

	public void balanceada(){
		if(fatorDeBalanceamento(raiz) <=-2 && fatorDeBalanceamento(raiz) >= 2){
			System.out.println("Arvore Balanceada!");
		}else{
			System.out.println("Arvore Desbalanceada!");
		}
	}

	public int fatorDeBalanceamento(NoArvore subRaiz){

		int alturaEsquerda = alturaSubArvore(subRaiz.esquerdo);
		int alturaDireita = alturaSubArvore(subRaiz.direito);
		int FB = alturaDireita - (alturaEsquerda);

		return FB;
	}

// ==============================================================================================================//
	public void removerPorValor(int valor) {
		NoArvore no_do_Valor = busca(raiz,valor);

		if(no_do_Valor != null){
			removerPorNO( no_do_Valor);
			System.out.println("O NO Composto Pelo Valor "+valor+" foi Removido com Sucesso!");

		}else{
			System.out.println("Valor N�o Encontrado!");
		}
	}


	private NoArvore removerPorNO(NoArvore noDeveraSerExcluso) {
        NoArvore novoArrange= null;
        NoArvore noPai = pegarNoPai(noDeveraSerExcluso,raiz); // pegar a referencia do pai(Pai do NO q sera removido)

        if(noPai!=null) {

            if (noFolha(noDeveraSerExcluso)) {  // NO � uma folha? ------ se SIM ------ remove a folha
                if( noPai.esquerdo == noDeveraSerExcluso){ //apagar a referencia do filho que devera ser removido
                    noPai.esquerdo = null;
                }else{
                    noPai.direito=null;
                }
                return balanceada(noPai);

            } else {// Nao � Folha, Entao possui quntos Filhos

                if (umFoilho(noDeveraSerExcluso)) { // possui um filho

                    if (noDeveraSerExcluso.esquerdo != null) { // encontar o lado do neto
                        novoArrange = noDeveraSerExcluso.esquerdo; // apagar a referencia do filho, e por a do neto
                        //noDeveraSerExcluso.esquerdo = null; // O valor a ser removido � eliminado
                    } else {
                        novoArrange = noDeveraSerExcluso.direito;
                        //noDeveraSerExcluso.direito = null;
                    }

                } else {// possui dois filho

                    if (noDeveraSerExcluso.esquerdo.direito != null) {
                        NoArvore folha = pegarFolha(noDeveraSerExcluso.esquerdo.direito);

                        novoArrange=noDeveraSerExcluso;
                        novoArrange.dado = folha.dado;
                        removerPorNO(folha);
                    } else {
                        novoArrange = noDeveraSerExcluso.esquerdo;
                        novoArrange.direito = noDeveraSerExcluso.direito;
                    }

                }

            }

            if( noPai.esquerdo == noDeveraSerExcluso) { //resolve a situacao da folha
                noPai.esquerdo=novoArrange;
            }else{
                noPai.direito=novoArrange;
            }

            return balanceada(novoArrange);
        }else{
			if (umFoilho(noDeveraSerExcluso)) { // possui um filho
				if (noDeveraSerExcluso.esquerdo != null) { // encontar o lado do neto
					noDeveraSerExcluso = noDeveraSerExcluso.esquerdo;

				}else {// possui dois filho
					noDeveraSerExcluso = noDeveraSerExcluso.direito;
				}

			}else {// possui dois filho

			}
		}

        raiz=novoArrange;
        return raiz;
	}




}
