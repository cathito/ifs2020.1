package Arvore_AVL;

import ArvoreBinaria.Arvore;

public class testeArvoreNormal {

    public static void main(String[] args) {
    	Arvore Nemeton = new Arvore();

        System.out.println("\n                  ESTRUTURA DE DADOS II!");

        System.out.println("\n                Inserindo Dados Na Arvore Binaria: \n");

        Nemeton.adicionarValor(500);
        Nemeton.adicionarValor(300);
        Nemeton.adicionarValor(550);
        Nemeton.adicionarValor(600);
        Nemeton.adicionarValor(200);
        Nemeton.adicionarValor(400);
        Nemeton.adicionarValor(100);
        Nemeton.adicionarValor(250);
        Nemeton.adicionarValor(350);
        Nemeton.adicionarValor(450);
        Nemeton.adicionarValor(150);
        Nemeton.adicionarValor(540);
        Nemeton.adicionarValor(545);

        System.out.println("\n=================================================================");


        System.out.println("\n\n                  Percorrer em Largura!");
        Nemeton.PercorrerEmLargura();

        System.out.println("\n\n                    Arvores Impressa!");
        System.out.println("\n             Elementos que Compoe a Arvore de Forma Ordenada: \n   ");
        Nemeton.emOrdem();

        System.out.println("\n=================================================================");



        System.out.println("\n\n                 Dimensoes e Capacidade!");

        Nemeton.Altura();

        Nemeton.numeroFolhas();

        Nemeton.totalElementos();

        System.out.println("\n\n=================================================================");

        System.out.println("\n\n                   Busca por Elemento!");

        Nemeton.buscarValor(10);

        Nemeton.buscarValor(100);

        Nemeton.buscarValor(350);

        Nemeton.buscarValor(400);

        Nemeton.buscarValor(600);

        Nemeton.buscarValor(1000);

        System.out.println("\n\n=================================================================" +
                "\n=================================================================\n\n");


        System.out.println("                 Remover Elemento!");

        // Folha removida com sucesso! ----------------- ok
        // Com um Filho removido com sucesso! ---------- ok
        // Com dois Filho removido com sucesso! -------- X -- Ex: quando remove a raiz, esta Pegando a folha Errada

        Nemeton.removerPorValor(150);
        Nemeton.removerPorValor(540);
        Nemeton.removerPorValor(550);


        //Nemeton.remover(450);

        //Nemeton.remover(300);

        //Nemeton.remover(500);

        //Nemeton.remover(150);


        System.out.println("\n=================================================================");


        System.out.println("\n\n                  Percorrer em Largura!");
        Nemeton.PercorrerEmLargura();

        System.out.println("\n\n                    Arvores Impressa!");
        System.out.println("\n             Elementos que Compoe a Arvore de Forma Ordenada: \n   ");
        Nemeton.emOrdem();

        System.out.println("\n=================================================================");



        System.out.println("\n\n                 Dimensoes e Capacidade!");

        Nemeton.Altura();

        Nemeton.numeroFolhas();

        Nemeton.totalElementos();

        System.out.println("\n\n=================================================================");

        System.out.println("\n\n                   Busca por Elemento!");

        Nemeton.buscarValor(550);




///=========================================================================================================================//

        /* // ====== Questao do professor ====== //

        Arvore questao = new Arvore();

        //(a+b*(c+d)*e)
         
        System.out.println(
				"Removido o Valor: " + valor + "\n\nImprimindo os Dados que Compoem a Arvore apos a remocao: ");
		emOrdem();
		System.out.println("\n----------------------------------------------------------------\n");

        */

//=========================================================================================================================//


    }


}
