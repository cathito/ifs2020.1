package Arvore_AVL;

public class Arvore {
	
	protected NoArvore raiz;
// ----------------------------------//

	protected int altramMaxima = 0;


	public boolean arvoreVazia() {
		return raiz == null;
	}

	public boolean subArvoreVazia(NoArvore raiz) {
		return raiz == null;
	}

	public boolean folha(NoArvore subRaiz) {
		return subRaiz.esquerdo == null && subRaiz.direito==null;
	}

// ==============================================================================================================//
// ==============================================================================================================//

	public void add(int dado) {
		add(raiz, dado);
		System.out.println("Valor: " + dado + ", foi Adicionando a Arvore!");
	}

	private NoArvore add(NoArvore subRaiz, int dado) {

		if (subRaiz == null) {
			subRaiz = new NoArvore(dado);
			if (raiz == null)
				raiz = subRaiz;

		} else if (dado < subRaiz.dado) {
			subRaiz.esquerdo = add(subRaiz.esquerdo, dado);
		} else {
			subRaiz.direito = add(subRaiz.direito, dado);
		}

		return subRaiz;
	}

//==============================================================================================================//
//==============================================================================================================//

	public void buscarValor(int valor) {
		System.out.println("\nBuscando valor: " + valor);
		if( busca(raiz, valor) != null) {
			System.out.println("Valor encontrado!");
		}else {
			System.out.println("Valor N�o Encontrado!");
		}
	}

	protected NoArvore busca(NoArvore subRaiz, int valor) {

		if (subRaiz == null) {
			return null;

		} else {
			if (valor < subRaiz.dado) {
				return busca(subRaiz.esquerdo, valor);

			} else if (valor > subRaiz.dado){
				return busca(subRaiz.direito, valor);
			}
			return subRaiz;
		}
	}


// ==============================================================================================================//
// ==============================================================================================================//


	public void altura() {

        if (arvoreVazia()) {
			System.out.println("A Arvore esta Vazia. Logo, a Mesma N�o Possui Altura!");
        } else {
            int alturaEncontrada = altura(raiz, 0);

            System.out.println("\nNa linguagem de maquina a altura da arvore �: " + alturaEncontrada);
            System.out.println("Na linguagem humana a altura da arvore �: " + (alturaEncontrada + 1) + "\n");
            this.altramMaxima = 0;
        }
	}

// --------------------------------------------------------------------------//

	protected int altura(NoArvore subRaiz, int point) {

		if (subRaiz.esquerdo != null) {
			altura(subRaiz.esquerdo, ++point);
			point--;
		}
		if (subRaiz.direito != null) {
			altura(subRaiz.direito, ++point);
			point--;
		}

		if (point > altramMaxima) {
			altramMaxima = point;
		}

		return altramMaxima;
	}
// --------------------------------------------------------------------------//

	public void numeroFolhas() {
		if (arvoreVazia()) {
			System.out.println("A Arvore esta Vazia. Logo, a Mesma N�o Possui Folhas a Serem Contabilizadas!");
		} else {
			System.out.println("A arvore possui um limite maximo de " + ((int) Math.pow(2, altura(raiz, 0))) + ", folhas!");
			this.altramMaxima = 0;
		}
	}
// --------------------------------------------------------------------------//

	public void totalElementos() {
		if (arvoreVazia()) {
			System.out.println("A Arvore esta Vazia. Logo, a Mesma N�o Possui Elementos a Serem Contabilizados!");
		} else {
			System.out.println("A arvore possui um limite maximo de " + (((int) Math.pow(2, altura(raiz, 0) + 1)) - 1) + ", elementos!");
			this.altramMaxima = 0;
		}
	}

// ==============================================================================================================//

	public void PercorrerEmLargura() {
		if (arvoreVazia()) {
			System.out.println("A Arvore esta Vazia. Logo, a Mesma N�o Possui Elementos a Serem Percorridods!");
		} else {
			FilaSE fila = new FilaSE();
			fila.Adicionar(raiz);
			PercorrerEmLargura(fila, fila.getInicio().dado);
		}
	}

	private void PercorrerEmLargura(FilaSE fila, NoArvore noAtual) {
		System.out.println(noAtual.dado);

		if (noAtual.esquerdo != null) {
			fila.Adicionar(noAtual.esquerdo);
		}
		if (noAtual.direito != null) {
			fila.Adicionar(noAtual.direito);
		}
		fila.Remover();

		if (fila.getInicio() != null) {
			PercorrerEmLargura(fila, fila.getInicio().dado);
		}

	}

// ==============================================================================================================//
// ==============================================================================================================//


	public void removerPorValor(int valor) {

		NoArvore no_do_Valor = busca(raiz,valor);

		if(no_do_Valor != null){

			removerPorNO( no_do_Valor);

			System.out.println("O NO Composto Pelo "+valor+" foi Removido com Sucesso!");

		}else{
			System.out.println("Valor N�o Encontrado!");
		}
	}


	private NoArvore removerPorNO(NoArvore noDeveraSerExcluso) {

		if( noFolha(noDeveraSerExcluso) ){  // NO � uma folha? ------ se SIM ------ remove a folha

			NoArvore noPai = pegarNoPai(noDeveraSerExcluso,raiz); // pegar a referencia do pai

			if( noPai.esquerdo == noDeveraSerExcluso){ //apagar a referencia do filho que devera ser removido
				noPai.esquerdo = null;
			}else{
				noPai.direito=null;
			}

		}else{// quntos Filhos

			if(umFoilho(noDeveraSerExcluso) ){ // possui um filho
				NoArvore noPai = pegarNoPai(noDeveraSerExcluso,raiz); // pegar a referencia do pai(Pai do NO q sera removido)

				if( noPai.esquerdo == noDeveraSerExcluso) { // O filho q sera removido esta a esquerda do pai

					if(noDeveraSerExcluso.esquerdo != null) { // encontar o lado do neto
						noPai.esquerdo = noDeveraSerExcluso.esquerdo; // apagar a referencia do filho, e por a do neto
						noDeveraSerExcluso.esquerdo=null; // O valor a ser removido � eliminado
					}else{
						noPai.esquerdo = noDeveraSerExcluso.direito;
						noDeveraSerExcluso.direito=null;
					}
				}else{ // O filho q sera removido esta a direita do pai
					if(noDeveraSerExcluso.esquerdo != null) { // encontar o lado do neto
						noPai.direito = noDeveraSerExcluso.esquerdo; // apagar a referencia do filho, e por a do neto
						noDeveraSerExcluso.esquerdo=null; // O valor a ser removido � eliminado
					}else{
						noPai.direito = noDeveraSerExcluso.direito;
						noDeveraSerExcluso.direito=null;
					}
				}

			}else{// possui dois filho
				NoArvore folha = pegarFolha(noDeveraSerExcluso);
				//System.out.println("A Ultima Folha Possui o Valor: "+folha.dado);
				removerPorNO(folha);
				noDeveraSerExcluso.dado=folha.dado;
			}

		}
		return raiz;
	}

/*--------------------------------------------------------------------------------------------*/

	public boolean noFolha(NoArvore noDeveraSerRemovido) {
		if(noDeveraSerRemovido.esquerdo == null && noDeveraSerRemovido.direito == null){
			return true;
		}
		return false;
	}
/*--------------------------------------------------------------------------------------------*/

	public boolean umFoilho(NoArvore noDeveraSerRemovido) {
		if(noDeveraSerRemovido.esquerdo != null && noDeveraSerRemovido.direito != null){
			return false;
		}
		return true;
	}
/*--------------------------------------------------------------------------------------------*/

	public NoArvore pegarNoPai(NoArvore filho, NoArvore noPai) {
        try {
            if ((noPai.esquerdo != filho) && (filho.dado < noPai.dado)) {
                return pegarNoPai(filho, noPai.esquerdo);

            } else if ((noPai.direito != filho) && filho.dado >= noPai.dado) {
                return pegarNoPai(filho, noPai.direito);
            }else{
                return noPai;
            }
        }catch (NullPointerException e){
            return null;
        }
	}
/*--------------------------------------------------------------------------------------------*/

	protected NoArvore pegarFolha(NoArvore raiz) {
        try {
            if (raiz.esquerdo != null) {
                return pegarFolha(raiz.esquerdo);

            } else if (raiz.direito != null) {
                return pegarFolha(raiz.direito);
            }

            return raiz;
        }catch (NullPointerException e){
            return null;
        }
	}



// ==============================================================================================================//
// ==============================================================================================================//



	public void emOrdem() {
		imprimiOrdenado(raiz);
	}

	private void imprimiOrdenado(NoArvore no) {
		if (no != null) {
			imprimiOrdenado(no.esquerdo);
			System.out.print(no.dado + ", ");
			imprimiOrdenado(no.direito);
		}
	}

// -----------------------------------------------------//
	public void emOrdeminverso() {
		imprimiOrdenadoinverso(raiz);
	}

	private void imprimiOrdenadoinverso(NoArvore no) {
		if (no != null) {
			imprimiOrdenado(no.direito);
			System.out.print(no.dado + ", ");
			imprimiOrdenado(no.esquerdo);
		}
	}

// ==============================================================================================================//
// ==============================================================================================================//

	

}
